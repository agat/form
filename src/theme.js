// @flow

export type ThemeT = {
    colors: {
        primary: string
    },
    breakpoints: {
        sm: number,
        md: number
    }
};

export default {
    colors: {
        primary: '#EE7023',
    },
    breakpoints: {
        sm: 600,
        md: 960,
    },
};
