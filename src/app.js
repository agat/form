// @flow
import * as React from 'react';
import styled, { ThemeProvider } from 'styled-components';
import theme from 'theme';

import Banner from 'components/Banner';


import Form from 'form';

const App = () => {
    const hours = new Date().getHours();
    const canFillForm = hours >= 7 && hours <= 22;

    return (
        <ThemeProvider theme={theme}>
            <Page>
                {canFillForm && (
                    <Form />
                )}
                {!canFillForm && (
                    <Banner>
                        With regard to the Law on Consumer Credit, you will be able to fill in an application from 7 am till 10 pm in Lithuanian time.
                    </Banner>
                )}
            </Page>
        </ThemeProvider>
    );
};

const Page = styled.div`
    padding: 24px;
    box-sizing: border-box;
    max-width: 760px;
    margin: auto;

    @media (max-width: ${props => props.theme.breakpoints.sm}px) {
        padding: 12px;
    }
`;

export default App;
