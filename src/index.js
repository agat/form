// @flow
import React from 'react';
import ReactDOM from 'react-dom';

import 'css-reset-and-normalize/css/reset-and-normalize.min.css';
import './index.css';

import App from './app';

ReactDOM.render(<App />, document.getElementById('root'));
