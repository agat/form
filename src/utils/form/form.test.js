// @flow

import {
    checkIfMonthlySalaryIsEnough,
} from './index';


test('Check if montly salary is enough', () => {
    expect(checkIfMonthlySalaryIsEnough(300, false)).toBeFalsy();
    expect(checkIfMonthlySalaryIsEnough(350, false)).toBeTruthy();
});

test('Check if montly salary with co-borrower is enough', () => {
    expect(checkIfMonthlySalaryIsEnough(350, true)).toBeFalsy();
    expect(checkIfMonthlySalaryIsEnough(700, true)).toBeTruthy();
});
