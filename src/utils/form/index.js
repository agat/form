/* eslint-disable max-len */
// @flow

export const checkIfMonthlySalaryIsEnough = (monthlySalary: number, hasCoBorrower: boolean) => {
    if (hasCoBorrower) {
        return monthlySalary >= 700;
    }

    return monthlySalary >= 350;
};

export default {
    checkIfMonthlySalaryIsEnough,
};
