// @flow
import * as React from 'react';

import {
    Headline4,
    Headline6,
} from 'components/Typography';
import Row from 'components/Row';
import Table from 'components/Table';
import Button from 'components/Button';

import {
    type FormT,
} from '../index';


type PropsT = {
    form: FormT,
    onBack: () => void
};

const Summary = ({ form, onBack }: PropsT) => (
    <>
        <Row space>
            <Headline4>
                Summary
            </Headline4>
        </Row>
        <Row space>
            <Headline6>
                Loan details
            </Headline6>
        </Row>
        <Row>
            <Table>
                <tbody>
                    <tr>
                        <th>Amount</th>
                        <td>
                            {`${form.loan.amount} €`}
                        </td>
                    </tr>
                    <tr>
                        <th>Term</th>
                        <td>
                            {`${form.loan.term} months`}
                        </td>
                    </tr>
                    <tr>
                        <th>Day of repayment</th>
                        <td>
                            {form.loan.repaymentDay}
                        </td>
                    </tr>
                    <tr>
                        <th>Monthly salary</th>
                        <td>
                            {`${form.loan.montlySalary} €`}
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Row>
        <Row space>
            <Headline6>
                Personal data
            </Headline6>
        </Row>
        <Row>
            <Table>
                <tbody>
                    <tr>
                        <th>Type of labour contract</th>
                        <td>
                            {form.personalInfo.labourContract}
                        </td>
                    </tr>
                    <tr>
                        <th>Marital status</th>
                        <td>
                            {form.personalInfo.martialStatus}
                        </td>
                    </tr>
                    <tr>
                        <th>Credit type</th>
                        <td>
                            {form.personalInfo.creditType}
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Row>
        {!!form.coBorrower.firstName && (
            <>
                <Row space>
                    <Headline6>
                        Co-borrower´s data
                    </Headline6>
                </Row>
                <Row>
                    <Table>
                        <tbody>
                            <tr>
                                <th>First name</th>
                                <td>
                                    {form.coBorrower.firstName}
                                </td>
                            </tr>
                            <tr>
                                <th>Last name</th>
                                <td>
                                    {form.coBorrower.lastName}
                                </td>
                            </tr>
                            <tr>
                                <th>Personal code</th>
                                <td>
                                    {form.coBorrower.personalCode}
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </Row>
            </>
        )}
        <Row space>
            <Headline6>
                Contact data
            </Headline6>
        </Row>
        <Row>
            <Table>
                <tbody>
                    <tr>
                        <th>Phone</th>
                        <td>
                            {form.personalInfo.phone}
                        </td>
                    </tr>
                    {!!form.personalInfo.comments.trim() && (
                        <tr>
                            <th>Comments</th>
                            <td>
                                {form.personalInfo.comments}
                            </td>
                        </tr>
                    )}
                </tbody>
            </Table>
        </Row>
        <Row space>
            <Button
                type="button"
                onClick={onBack}
            >
                Back
            </Button>
        </Row>
    </>
);

export default Summary;
