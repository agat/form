// @flow
import * as React from 'react';

import Row from 'components/Row';
import Button from 'components/Button';
import {
    Headline4,
    Text,
} from 'components/Typography';
import FormStepper from './FormStepper';

import LoanForm from './loanForm';
import PersonalForm from './PersonalForm';
import Summary from './Summary';

export type LoanT = {
    amount: string,
    term: string,
    repaymentDay: string,
    montlySalary: string
};

export type PersonalInfoT = {
    labourContract: string,
    martialStatus: string,
    creditType: string,
    phone: string,
    comments: string
};

export type CoBorrowerT = {
    firstName: string,
    lastName: string,
    personalCode: string
};

export type FormT = {
    loan: LoanT,
    personalInfo: PersonalInfoT,
    coBorrower: CoBorrowerT
};

export type PersonalFormT = {
    personalInfo: PersonalInfoT,
    coBorrower: CoBorrowerT
};

const STEPS = [
    'Welcome',
    'Loan details',
    'Personal details',
    'Summary',
];

export type StateT = {
    currentStepIndex: number,
    completedStepsCount: number,
    form: FormT
};

const Form = () => {
    const [state, updateState] = React.useState({
        currentStepIndex: 0,
        steps: STEPS,
        completedStepsCount: 0,
        form: {
            loan: {
                amount: '300',
                term: '6',
                repaymentDay: '3',
                montlySalary: '',
            },
            personalInfo: {
                labourContract: 'Open-end',
                martialStatus: 'Single',
                creditType: 'Personal credit',
                phone: '',
                comments: '',
            },
            coBorrower: {
                firstName: '',
                lastName: '',
                personalCode: '',
            },
        },
    });
    const onFormBack = (formState: Object) => {
        updateState({
            ...state,
            currentStepIndex: state.currentStepIndex - 1,
            form: {
                ...state.form,
                ...formState,
            },
        });
    };
    const onSummaryBack = () => {
        updateState({
            ...state,
            currentStepIndex: state.currentStepIndex - 1,
        });
    };
    const onFormForward = (formState: Object, completedStepsCount: number) => {
        updateState({
            ...state,
            currentStepIndex: state.currentStepIndex + 1,
            completedStepsCount,
            form: {
                ...state.form,
                ...formState,
            },
        });
    };

    return (
        <>
            <Row>
                <FormStepper
                    steps={STEPS}
                    currentStepIndex={state.currentStepIndex}
                    completedStepsCount={state.completedStepsCount}
                />
            </Row>
            {state.currentStepIndex === 0 && (
                <>
                    <Row space>
                        <Headline4>Small loan application</Headline4>
                    </Row>
                    <Row>
                        <Text>
                            Please click Forward button to process.
                        </Text>
                    </Row>
                    <Row
                        align="right"
                    >
                        <Button
                            onClick={() => updateState({
                                ...state,
                                currentStepIndex: 1,
                                completedStepsCount: 1,
                            })}
                        >
                            Forward
                        </Button>
                    </Row>
                </>
            )}
            {state.currentStepIndex === 1 && (
                <LoanForm
                    form={state.form}
                    onBack={onFormBack}
                    onForward={form => onFormForward(form, 2)}
                />
            )}
            {state.currentStepIndex === 2 && (
                <PersonalForm
                    form={state.form}
                    onBack={onFormBack}
                    onForward={form => onFormForward(form, 3)}
                />
            )}
            {state.currentStepIndex === 3 && (
                <Summary
                    form={state.form}
                    onBack={onSummaryBack}
                />
            )}
        </>
    );
};

export default Form;
