// @flow

import * as React from 'react';
import { useFormState } from 'react-use-form-state';

import { Headline4 } from 'components/Typography';
import InputWithlabel from 'components/InputWithlabel';
import Button from 'components/Button';
import Row from 'components/Row';
import Columns from 'components/Columns';

import {
    type LoanT,
    type FormT,
} from '../index';


type PropsT = {
    form: FormT,
    onForward: (form: Object) => void,
    onBack: (form: Object) => void,
};

const LoanForm = ({
    form,
    onForward,
    onBack,
}: PropsT) => {
    const [formState, { number, select }] = useFormState<LoanT>(form.loan);
    const handleSubmit = (e: SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault();
        e.stopPropagation();

        onForward({
            loan: formState.values,
        });
    };

    // console.info(formState);

    return (
        <form onSubmit={handleSubmit}>
            <Row space>
                <Headline4>Loan details</Headline4>
            </Row>
            <Row>
                <InputWithlabel
                    id="amount"
                    isText
                    label="Loan amount(€)*"
                    inputProps={{
                        ...number('amount'),
                        required: true,
                        min: 300,
                        max: 20000,
                        step: 100,
                        autoFocus: true,
                    }}
                    error={formState.errors.amount}
                    description="From 300 € to 20000 €"
                />
            </Row>
            <Row>
                <InputWithlabel
                    id="term"
                    isText
                    label="Loan term(months)*"
                    inputProps={{
                        ...number('term'),
                        required: true,
                        min: 6,
                        max: 60,
                        step: 1,
                    }}
                    error={formState.errors.term}
                    description="From 6 to 60 months"
                />
            </Row>
            <Row>
                <InputWithlabel
                    id="repaymentDay"
                    label="Day of repayment"
                    isSelect
                    description="Day of repayment of principal and payment of interest"
                    inputProps={select('repaymentDay')}
                >
                    <option>3</option>
                    <option>7</option>
                    <option>12</option>
                    <option>17</option>
                    <option>22</option>
                    <option>27</option>
                </InputWithlabel>
            </Row>
            <Row space>
                <Headline4>
                    Obligations data
                </Headline4>
            </Row>
            <Row>
                <InputWithlabel
                    id="montlySalary"
                    isText
                    label="Monthly salary(€)*"
                    inputProps={{
                        ...number('montlySalary'),
                        required: true,
                        min: 350,
                    }}
                    error={formState.errors.montlySalary}
                    description={'Indicate the earnings which you receive in "hands" (after taxes) per month.'}
                />
            </Row>
            <Row space>
                <Columns isResponsive={false}>
                    <Columns.Col>
                        <Button
                            type="button"
                            onClick={() => onBack({
                                loan: formState.values,
                            })}
                        >
                            Back
                        </Button>
                    </Columns.Col>
                    <Columns.Col
                        isNarrow
                        isResponsive={false}
                    >
                        <Button
                            type="submit"
                            isPrimary
                        >
                            Forward
                        </Button>
                    </Columns.Col>
                </Columns>
            </Row>
        </form>
    );
};

export default LoanForm;
