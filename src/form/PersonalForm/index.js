/* eslint-disable react/jsx-one-expression-per-line */
// @flow

import * as React from 'react';
import { useFormState } from 'react-use-form-state';

import { Headline4 } from 'components/Typography';
import InputWithlabel from 'components/InputWithlabel';
import Button from 'components/Button';
import Row from 'components/Row';
import Columns from 'components/Columns';
import Banner from 'components/Banner';

import { checkIfMonthlySalaryIsEnough } from 'utils/form';

import {
    type FormT,
    type PersonalInfoT,
    type CoBorrowerT,
    type PersonalFormT,
} from '../index';


type PropsT = {
    form: FormT,
    onForward: (form: PersonalFormT) => void,
    onBack: (form: PersonalFormT) => void,
};

type SimplePersonalFormT = PersonalInfoT & CoBorrowerT;

const convertFormState = ({
    labourContract,
    martialStatus,
    creditType,
    phone,
    comments,
    firstName,
    lastName,
    personalCode,
}: SimplePersonalFormT): PersonalFormT => ({
    personalInfo: {
        labourContract,
        martialStatus,
        creditType,
        phone,
        comments,
    },
    coBorrower: {
        firstName,
        lastName,
        personalCode,
    },
});

const PersonalForm = ({
    form,
    onForward,
    onBack,
}: PropsT) => {
    const [isMontlySalaryLow, setMontlySalaryIsLow] = React.useState(false);
    const [isCoBorrower, setCoBorrowerVisibility] = React.useState(false);
    const [formState, {
        select,
        tel,
        text,
        number,
        textarea,
    }] = useFormState<PersonalFormT>({
        ...form.personalInfo,
        ...form.coBorrower,
    });
    const addCoBorrower = () => setCoBorrowerVisibility(true);
    const removeCoBorrower = () => {
        setCoBorrowerVisibility(false);

        formState.clearField('firstName');
        formState.clearField('lastName');
        formState.clearField('personalCode');
    };
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault();
        e.stopPropagation();

        if (isMontlySalaryLow) {
            return;
        }

        onForward(convertFormState(formState.values));
    };

    React.useEffect(() => {
        setMontlySalaryIsLow(!checkIfMonthlySalaryIsEnough(form.loan.montlySalary, isCoBorrower));
    }, [
        isCoBorrower,
        form.loan.montlySalary,
    ]);

    return (
        <form onSubmit={handleSubmit}>
            <Row space>
                <Headline4>Personal data</Headline4>
            </Row>
            <Row>
                <InputWithlabel
                    id="labourContract"
                    label="Type of labour contract"
                    isSelect
                    inputProps={{
                        ...select('labourContract'),
                        autoFocus: true,
                    }}
                >
                    <option>Open-end</option>
                    <option>Fixed term</option>
                    <option>Other</option>
                </InputWithlabel>
            </Row>
            <Row>
                <InputWithlabel
                    id="martialStatus"
                    label="Marital status"
                    isSelect
                    inputProps={select('martialStatus')}
                >
                    <option>Single</option>
                    <option>Married</option>
                    <option>Common law marriage</option>
                    <option>Divorced</option>
                    <option>Widow/Widower</option>
                </InputWithlabel>
            </Row>
            <Row>
                <InputWithlabel
                    id="creditType"
                    label="Credit type"
                    isSelect
                    inputProps={select('creditType')}
                >
                    <option>Personal credit</option>
                    <option>Family/household credit</option>
                </InputWithlabel>
            </Row>
            <Row space>
                <Headline4>Co-borrower´s data</Headline4>
            </Row>
            {!isCoBorrower && (
                <Row
                    align="right"
                >
                    <Button
                        onClick={addCoBorrower}
                        type="button"
                    >
                        Add Co-applicant
                    </Button>
                </Row>
            )}
            {isCoBorrower && (
                <>
                    {isMontlySalaryLow && (
                        <Row>
                            <Banner>
                                If you take a loan with a co-borrower, total income of a family (co-borrower) must be at least 700 EUR per month after tax. Now is {form.loan.montlySalary} €.
                            </Banner>
                        </Row>
                    )}
                    <Row>
                        <InputWithlabel
                            id="firstName"
                            label="First name*"
                            isText
                            inputProps={{
                                ...text('firstName'),
                                required: true,
                                autoFocus: true,
                            }}
                            error={formState.errors.firstName}
                        />
                    </Row>
                    <Row>
                        <InputWithlabel
                            id="lastName"
                            label="Last name*"
                            isText
                            inputProps={{
                                ...text('lastName'),
                                required: true,
                            }}
                            error={formState.errors.lastName}
                        />
                    </Row>
                    <Row>
                        <InputWithlabel
                            id="personalCode"
                            label="Personal code*"
                            isText
                            inputProps={{
                                ...number('personalCode'),
                                required: true,
                            }}
                            error={formState.errors.personalCode}
                        />
                    </Row>
                    <Row
                        align="right"
                    >
                        <Button
                            onClick={removeCoBorrower}
                            type="button"
                        >
                            Remove Co-applicant
                        </Button>
                    </Row>
                </>
            )}
            <Row space>
                <Headline4>Contact data</Headline4>
            </Row>
            <Row>
                <InputWithlabel
                    id="phone"
                    label="Phone*"
                    isText
                    inputProps={{
                        ...tel('phone'),
                        required: true,
                    }}
                    error={formState.errors.phone}
                />
            </Row>
            <Row>
                <InputWithlabel
                    id="comments"
                    label="Additional information"
                    isMultiline
                    inputProps={textarea('comments')}
                />
            </Row>
            <Row space>
                <Columns isResponsive={false}>
                    <Columns.Col>
                        <Button
                            type="button"
                            onClick={() => onBack(convertFormState(formState.values))}
                        >
                            Back
                        </Button>
                    </Columns.Col>
                    <Columns.Col
                        isNarrow
                        isResponsive={false}
                    >
                        <Button
                            type="submit"
                            isPrimary
                            disabled={isMontlySalaryLow}
                        >
                            Forward
                        </Button>
                    </Columns.Col>
                </Columns>
            </Row>
        </form>
    );
};

export default PersonalForm;
