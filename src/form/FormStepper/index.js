// @flow
import * as React from 'react';

import {
    Stepper,
    Step,
} from 'components/Stepper';


type PropsT = {
    steps: string[],
    currentStepIndex: number,
    completedStepsCount: number,
};

const FormStepper = ({
    steps,
    currentStepIndex,
    completedStepsCount,
}: PropsT) => (
    <Stepper>
        {steps.map((title, index) => (
            <Step
                key={title}
                isCurrent={index === currentStepIndex}
                isCompleted={index + 1 <= completedStepsCount}
            >
                {title}
            </Step>
        ))}
    </Stepper>
);

export default FormStepper;
