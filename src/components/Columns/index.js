/* eslint-disable no-confusing-arrow */
// @flow

import * as React from 'react';
import styled, { css } from 'styled-components';

import { type ThemeT } from 'theme';

type PropsT = {
    align?: 'flex-start' | 'flex-end' | 'center',
    contentAlign?: string,
    isResponsive?: boolean,
    theme: ThemeT
}

const Columns: React.ComponentType<PropsT> = styled.div`
    display: flex;
    align-items: ${props => props.align || 'center'};
    justify-content: ${props => props.contentAlign || 'start'};

    ${props => props.isResponsive && css`
        @media (max-width: ${props.theme.breakpoints.sm}px) {
            display: block;
        }
    `}
`;

Columns.defaultProps = {
    isResponsive: true,
};


type ColPropsT = {
    isNarrow?: boolean,
    isResponsive?: boolean,
    theme: ThemeT
}

const Col: React.ComponentType<ColPropsT> = styled.div`
    flex: ${props => props.isNarrow ? 'none' : '1'};
    padding-right: 16px;

    &:first-child {
        padding-top: 0;
    }

    &:last-child {
        padding-right: 0;
    }

    ${props => props.isResponsive && css`
        @media (max-width: ${props.theme.breakpoints.sm}px) {
            padding-right: 0;
            padding-top: 16px;
        }
    `}
`;

Col.defaultProps = {
    isResponsive: true,
};

Columns.Col = Col;

export default Columns;
