// @flow

import * as React from 'react';
import styled, { css } from 'styled-components';


type PropsT = {
    isPrimary?: boolean
};

const Button: React.ComponentType<PropsT> = styled.button`
    color: ${props => props.theme.colors.primary};
    display: inline-flex;
    align-items: center;
    justify-content: center;
    position: relative;
    min-width: 88px;
    height: 36px;
    padding: 0 14px;
    border: 2px solid currentColor;
    border-radius: 4px;
    outline: 0;
    background: transparent;
    font-size: 14px;
    font-weight: 500;
    line-height: 32px;
    text-align: center;
    text-decoration: none;
    text-transform: uppercase;
    overflow: hidden;
    vertical-align: middle;
    user-select: none;
    box-sizing: border-box;
    -webkit-appearance: none;

    ${props => props.isPrimary && css`
        color: white;
        background: ${props.theme.colors.primary};
        border-color: ${props.theme.colors.primary};
    `}
    
    &:active {
        outline: 0;
    }

    &:focus {
        outline: 0;
    }

    &:active,
    &:hover {
        cursor: pointer;
        color: white;
        background: ${props => props.theme.colors.primary};
        border-color: ${props => props.theme.colors.primary};
    }
`;

Button.defaultProps = {
    isPrimary: false,
};

export default Button;
