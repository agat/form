/* eslint-disable no-confusing-arrow */
import * as React from 'react';

import styled from 'styled-components';
import { Headline6 } from 'components/Typography';


const Line = styled.div`
    flex: 1;
    border-top: 1px solid ${props => props.theme.colors.primary};
    margin: 0 16px;

    @media (max-width: ${props => props.theme.breakpoints.sm}px) {
        margin: 0 8px;
    }
`;

const StepperBody = styled.div`
    display: flex;
    align-items: center;
    flex-wrap: wrap;
`;

type PropsT = {
    children: React.Node,
};

export const Stepper = ({
    children,
}: PropsT) => {
    const childrenArray = [];

    React.Children.forEach(children, (child, index) => {
        childrenArray.push(child);

        if (index + 1 < children.length && index + 1 !== children.length) {
            childrenArray.push(<Line />);
        }
    });

    return (
        <StepperBody>
            {React.Children.map(childrenArray, child => child)}
        </StepperBody>
    );
};

type StepPropsT = {
    isCurrent?: boolean,
    isCompleted?: boolean
};

export const Step: React.ComponentType<StepPropsT> = styled(Headline6)`
    color: ${props => props.isCurrent ? props.theme.colors.primary : 'inherit'};
    opacity: ${props => props.isCompleted || props.isCurrent ? 1 : 0.5};

    @media (max-width: ${props => props.theme.breakpoints.sm}px) {
        font-size: 14px;
    }
`;

export default {
    Stepper,
    Step,
};
