// @flow

import * as React from 'react';
import styled from 'styled-components';

import { type ThemeT } from 'theme';


type PropsT = {
    theme: ThemeT
}

const Select: React.ComponentType<PropsT> = styled.select`
    appearance: none;
    display: block;
    width: 100%;
    outline: 0;
    height: 56px;
    font-size: 20px;
    border-radius: 4px;
    padding: 0 16px;
    border: 2px solid currentColor;
    cursor: pointer;

    &:focus,
    &:hover {
        border-color: ${props => props.theme.colors.primary};
    }
`;

export default Select;
