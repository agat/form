import * as React from 'react';
import styled from 'styled-components';

import Input from 'components/Input';
import InputMultiline from 'components/InputMultiline';
import Select from 'components/Select';
import Columns from 'components/Columns';
import {
    Label,
    Text,
} from 'components/Typography';

type PropsT = {
    id: string,
    label: string,
    inputProps: Object,
    isText?: boolean,
    isSelect?: boolean,
    isMultiline?: boolean,
    error?: ?string,
    description?: ?string,
    children?: ?React.Node
};

const InputWithlabel = ({
    label,
    inputProps,
    id,
    error,
    isText,
    isSelect,
    isMultiline,
    children,
    description,
}: PropsT) => (
    <Columns
        align="flex-start"
    >
        <LabelCol
            narrow
        >
            <Label
                htmlFor={id}
            >
                {`${label}:`}
            </Label>
        </LabelCol>
        <Columns.Col>
            {isText && (
                <Input
                    id={id}
                    {...inputProps}
                />
            )}
            {isMultiline && (
                <InputMultiline
                    id={id}
                    {...inputProps}
                />
            )}
            {isSelect && (
                <Select
                    id={id}
                    {...inputProps}
                >
                    {children}
                </Select>
            )}
            {!!description && (
                <DescriptionText>
                    {description}
                </DescriptionText>
            )}
            {!!error && (
                <ErrorText>
                    {error}
                </ErrorText>
            )}
        </Columns.Col>
    </Columns>
);

InputWithlabel.defaultProps = {
    error: '',
    description: '',
    isText: false,
    isSelect: false,
    isMultiline: false,
    children: null,
};

const LabelCol = styled(Columns.Col)`
    width: 30%;
    margin-top: 16px;

    @media (max-width: ${props => props.theme.breakpoints.sm}px) {
        width: auto;
    }
`;

const DescriptionText = styled(Text)`
    font-size: 14px;
    line-height: 20px;
    margin-top: 8px;
`;

const ErrorText = styled(DescriptionText)`
    color: red;
`;

export default InputWithlabel;
