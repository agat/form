// @flow

import styled from 'styled-components';


const Box = styled.div`
    margin: 24px 16px;
`;

export default Box;
