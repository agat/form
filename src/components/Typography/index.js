/* eslint-disable no-confusing-arrow */
// @flow

import styled from 'styled-components';

import { type ThemeT } from 'theme';


export const Headline4 = styled.h2`
    color: ${props => props.theme.colors.primary};
    font-size: 34px;
    line-height: 40px;
    font-weight: 400;

    @media (max-width: ${props => props.theme.breakpoints.sm}px) {
        font-size: 28px;
    }
`;

export const Headline5 = styled.h2`
    color: ${props => props.theme.colors.primary};
    font-size: 24px;
    line-height: 32px;
    font-weight: 400;

    @media (max-width: ${props => props.theme.breakpoints.sm}px) {
        font-size: 20px;
    }
`;

export const Headline6 = styled.h2`
    color: ${props => props.theme.colors.primary};
    font-size: 20px;
    line-height: 32px;
    font-weight: 400;

    @media (max-width: ${props => props.theme.breakpoints.sm}px) {
        font-size: 18px;
    }
`;

export const Text = styled.p`
    font-size: ${props => props.isSmall ? '12px' : '16px'};
    line-height: 24px;
`;

type LabelPropsT = {
    theme: ThemeT
}

export const Label: React.ComponentType<LabelPropsT> = styled.label`
    display: block;
    font-size: 18px;
    line-height: 24px;
    text-align: right;

    @media (max-width: ${props => props.theme.breakpoints.sm}px) {
        text-align: left;
    }
`;

export default {
    Headline4,
    Headline5,
    Text,
    Label,
};
