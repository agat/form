// @flow

import * as React from 'react';
import styled from 'styled-components';


type PropsT = {};

const Table: React.ComponentType<PropsT> = styled.table`
    width: 100%;
    
    tbody {
        font-size: 16px;
    }

    th {
        opacity: .60;
    }

    tr th:first-child {
        /* width: 40%; */
    }

    tr {
        display: flex;
        border-bottom: 1px solid rgba(0, 0, 0, .12);
        padding: 12px 0;

        @media (max-width: ${props => props.theme.breakpoints.sm}px) {
            flex-direction: column;
            padding: 8px 0;
        }
    }

    th, td {
        flex: 1;
        padding: 0 16px;

        @media (max-width: ${props => props.theme.breakpoints.sm}px) {
            padding: 0;
        }
    }
`;

export default Table;
