// @flow

import styled from 'styled-components';


const InputMultiline = styled.textarea`
    display: block;
    width: 100%;
    outline: 0;
    min-height: 56px;
    line-height: 1.4;
    font-size: 20px;
    border-radius: 4px;
    padding: 16px;
    border: 2px solid currentColor;

    &:focus,
    &:hover {
        border-color: ${props => props.theme.colors.primary};
    }
`;

export default InputMultiline;
