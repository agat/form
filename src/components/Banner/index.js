// @flow

import styled from 'styled-components';


const Banner = styled.div`
    color: white;
    background: ${props => props.theme.colors.primary};
    padding: 12px;
    font-size: 16px;
`;

export default Banner;
